### 架構

分成兩部分，Component 和 Store 來描述。

#### Component

- Main：定義 route
- App：HeroCardList，也可以視為 HeroCardIndex，在這次的情況可以算是一個 APP ，所以就叫 App 。
- HeroCard：定義 hero card。
- Profile：定義 profile 和處理事件。

#### Store

- callApi：加上了一個 middleware 處理 request ，在發 request 的時候，可以接連產生 `${evt}-PENDING` ， `${evt}-DONE` ， `${evt}-FAIL` ， `${evt}-ALWAYS` 的事件，雖然不夠完整，不過大致上就是做這樣的事情。
- listReducer：真正的資料處理在這裡。
- listAction：這裡定義的會被 component 拿來使用，算是 component 和 store 的橋樑。

### 第三方 library

#### yeoman 和 generator-react-webpack

yeoman 就是拿來快速建立專案的工具，這次搭配 generator-react-webpack 使用。

#### React

對於 component 的 template 和 event 的定義，我認為是目前用過最清楚的，也很好搭配各種不同的 store library 使用，不一定要使用 fluxxor 、 reflux 或者 Redux，彈性很大。而且、可以很容易專注在某個區域開發或優化。

#### React-Router

可以搭配 React 處理 route 的 plugin。不需要再煩惱 IE 。

#### Redux

處理資料的 library ，相較 fluxxor 或 reflux ，Redux 減少了很多重複性的寫法，使用上看起來就比較簡潔，也有非常方便的 debug 工具可以使用。在銜接 React component 的時候也清晰很多。而且還蠻容易達成 code reused 的。

#### bootstrap

這次只用了 style 的部分。雖然、有越來越多輕量的 library ，但目前比較熟悉它。

### 關於註解

我認為一個程式需要真正寫下來的註解並不多，除非是為了導入`jsdoc` 、 `esdoc` 或者為了導入 type ，而需要使用註解。因此、撇開上述的需求，大致上有：

- 複雜的條件，或者條件經過簡化。
- 類似像使用了泛型寫法，而無法一眼看穿如何使用的情況。
- 只為解決某些特殊情況。

### 其他

- 因為不太熟悉 webpack ，在加入 `bootstrap-loader` 的時候出些問題，但看著錯誤訊息一個個解掉了。
- 同樣是不太熟悉 webpack 打包 css ，也因為有導入 bootstrap ，因此就直接寫 css ，沒有使用 scss 了。


'use strict';

import React from 'react';
import { connect } from 'react-redux';
import HeroCard from './HeroCard';
import * as actions from '../stores/listActions';

export class App extends React.Component {
  componentDidMount() {
    this.props.getList({
      endpoint: 'http://hahow-recruit.herokuapp.com/heroes'
    });
  }

  render() {
    const { heros } = this.props;
    if (!heros) { return false; }
    return (
      <div className="row">
        <div className="row hero-list">
          { heros.map((hero, idx) => (
            <HeroCard
              key={`hero-${idx}`}
              hero={hero}
            />
          )) }
        </div>
        {this.props.children}
      </div>
    );
  }
}

// about store
const mapState2Props = (state) => ({
  heros: state.heros || []
});

const mapDispatch2Props = dispatch => ({
  getList(props) {
    return new Promise((resolve) => {
      dispatch(actions.getList(props));
      resolve();
    });
  }
});

export default connect(mapState2Props, mapDispatch2Props)(App);

'use strict';

import React from 'react';
import { Link } from 'react-router'

export default class HeroCard extends React.Component {
  render() {
    const { hero } = this.props;
    return (
      <div className="col-xs-12 col-sm-3 text-center">
        <Link
          activeClassName="active"
          className="hero-card"
          to={`/heros/${hero.id}`}
        >
          <div className="img-container">
            <img className="img-responsive img-thumbnail" src={hero.image} alt='hero-avatar' />
          </div>
          <p>
            {hero.name}
          </p>
        </Link>
      </div>
    );
  }
}

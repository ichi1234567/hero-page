'use strict';

import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../stores/listActions';

export class Profile extends React.Component {
  constructor(...args) {
    super(...args);

    ['str', 'int', 'agi', 'luk'].forEach((key) => {
      this[`onClickAdd${key}`] = this.onClickAdd.bind(this, key);
      this[`onClickSub${key}`] = this.onClickSub.bind(this, key);
    });
    this.onClickSave = this.onClickSave.bind(this);
  }

  componentDidMount() {
    const data = this.props.routeParams;
    this.loadItem(data);
  }

  componentWillReceiveProps(nextProps) {
    const thisData = this.props.routeParams;
    const nextData = nextProps.routeParams;
    if (thisData.id !== nextData.id || !this.props.profile) {
      this.loadItem(nextData);
    }
  }

  loadItem(data) {
    this.props.getItem(data.id, {
      endpoint: `http://hahow-recruit.herokuapp.com/heroes/${data.id}/profile`
    });
  }

  onClickAdd(name) {
    const { profile } = this.props;
    this.props.updateItem(
      profile.id,
      {
        [name]: profile[name] + 1,
        total: profile.total - 1
      }
    );
  }

  onClickSub(name) {
    const { profile } = this.props;
    this.props.updateItem(
      profile.id,
      {
        [name]: profile[name] - 1,
        total: profile.total + 1
      }
    );
  }

  onClickSave() {
    const { profile } = this.props;
    let params = {
      endpoint: `https://hahow-recruit.herokuapp.com/heroes/${profile.id}/profile`,
      method: 'PATCH'
    };
    let formData = {};
    ['str', 'int', 'agi', 'luk'].forEach(
      (name) => {
        formData = Object.assign({ [name]: profile[name] }, formData);
      }
    );
    params.params = formData;
    if (profile.total === 0) {
      this.props.saveItem(profile.id, params);
    }
  }

  renderAbility(name, value = 0, total = 0) {
    return (
      <div className="ablility" key={`profile-${name}`}>
        <strong className="ablility-name">{name.toUpperCase()}：</strong>
        {value > 0 ?
          (<button className="btn btn-default" onClick={this[`onClickSub${name}`]}>-</button>) :
          (<span className="btn btn-default disabled">-</span>)}
        <span className="ablility-value text-center">{value}</span>
        {total > 0 ?
          (<button className="btn btn-default" onClick={this[`onClickAdd${name}`]}>+</button>) :
          (<span className="btn btn-default disabled">+</span>)}
      </div>
    );
  }

  render() {
    const { profile } = this.props;
    if (!profile) { return false; }
    const nodes = Object.keys(profile)
      .filter(item => (item !== 'total' && item !== 'id'))
      .map((item) => (this.renderAbility(item, profile[item], profile.total)));

    return (
      <div className="row hero-profile">
        <div className="col-xs-12 col-sm-8 profile-content">
          {nodes}
        </div>
        <div className="col-xs-12 col-sm-4 profile-footer">
          <p>剩餘點數<span>{profile.total}</span></p>
          <button className="btn btn-primary" onClick={this.onClickSave}>儲存</button>
        </div>
      </div>
    );
  }
}

// about store
const mapState2Props = (state) => {
    return {
      profile: state.current || null
    };
};

const mapDispatch2Props = dispatch => ({
  getItem(...args) {
    return new Promise((resolve) => {
      dispatch(actions.getItem(...args));
      resolve();
    });
  },
  updateItem(...args) {
    return new Promise((resolve) => {
      dispatch(actions.updateItem(...args));
      resolve();
    });
  },
  saveItem(...args) {
    return new Promise((resolve) => {
      dispatch(actions.saveItem(...args));
      resolve();
    });
  }
});

export default connect(mapState2Props, mapDispatch2Props)(Profile);

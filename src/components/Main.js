require('normalize.css/normalize.css');
require('bootstrap-loader');
require('styles/App.css');

import React from 'react';
import { browserHistory, Router, Route } from 'react-router';
import App from './App';
import Profile from './Profile';

// const createIns = require('../lib/createIns');
class AppRoute extends React.Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Route path="/" component={App}>
          <Route path="heros/:id" component={Profile} />
        </Route>
        <Route path="/heros" component={App}>
          <Route path="heros/:id" component={Profile} />
        </Route>
      </Router>
    );
  }
}

AppRoute.defaultProps = {
};

export default AppRoute;

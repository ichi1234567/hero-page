import 'core-js/fn/object/assign';
import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import callApi from './stores/callApi';
import listReducer from './stores/listReducer';
import AppRoute from './components/Main';

const store = createStore(
  listReducer,
  {},
  applyMiddleware(thunk, callApi)
);

// Render the main component into the dom
ReactDOM.render(
  (<Provider store={store}>
     <AppRoute />
  </Provider>),
  document.getElementById('app')
);


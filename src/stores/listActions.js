'use strict';

export function getList(params) {
  return Object.assign({
    type: 'GET_LIST'
  }, params);
}

export function getItem(id, params) {
  return Object.assign({
    id,
    type: 'GET_ITEM'
  }, params);
}

export function updateItem(id, params) {
  return Object.assign({
    id,
    type: 'UPDATE_ITEM'
  }, params);
}

export function saveItem(id, params) {
  return Object.assign({
    id,
    type: 'SAVE_ITEM'
  }, params);
}

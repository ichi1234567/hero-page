'use strict';

const initState = {
  current: {},
  heros: []
};

export default function heroListReducer(state=initState, action) {
  const actionType = action.type;
  let newAction = Object.assign({}, action);
  delete newAction.type;
  newAction = Object.assign({}, newAction);

  switch(actionType) {
    case 'GET_LIST_DONE':
      if (!action.resp.slice) { return state; }
      return Object.assign({}, action.resp, {
        heros: action.resp.slice(0)
      });
    case 'GET_ITEM_DONE':
      if (state.current && state.current.id === newAction.id) {
        return state;
      }
      const current = Object.assign({
        id: newAction.id,
        total: 0
      }, action.resp);
      return Object.assign({}, state, { current });
    case 'UPDATE_ITEM':
      if (state.current.id !== newAction.id) {
        return state;
      }
      return Object.assign({}, state, {
        current: Object.assign({}, state.current, newAction)
      });
    default:
      return state;
  }
}


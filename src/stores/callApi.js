'use strict';

export default function callApi() {
  return (next) => (
    (action) => {
      const { endpoint } = action;
      if (endpoint === undefined) {
        return next(action);
      }

      function actionWith(data = {}) {
        const finalAction = Object.assign({}, data);
        delete finalAction.endpoint;
        return finalAction;
      }

      const { id, method, params } = action;
      next(actionWith({ id, type: `${action.type}_PENDING` }));
      const data = params || null;

      const req = new XMLHttpRequest();
      req.onreadystatechange = (e) => {
        const { target } = e;
        const resp = target.responseText ? JSON.parse(target.responseText) : {};
        if (target.readyState === XMLHttpRequest.DONE && target.status === 200) {
          next(actionWith({ id, resp, type: `${action.type}_DONE` }));
        }
        next(actionWith({ id, resp, type: `${action.type}_ALWAYS` }));
      };
      req.open(
        method || 'GET',
        endpoint
      );
      req.setRequestHeader('Content-Type', 'application/json');
			req.setRequestHeader('Accept', 'application/json');

      req.send((data && JSON.stringify(data)) || null);
      return this;
    }
  )
}
